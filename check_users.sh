#!/bin/bash
USER_FILE="/home/imbusch/git/overview-linux-user-groups/users.txt"
TEMP_FILE="/home/imbusch/git/overview-linux-user-groups/temp.txt"
who | awk -F " " '{print $1}' >> $USER_FILE
cat $USER_FILE | sort | uniq > $TEMP_FILE
mv $TEMP_FILE $USER_FILE
# TODO: this doesn't work anymore!
# try make use of getent group OE0219
