import argparse
import subprocess
import os

# extract all existing users using /etc/passwd
# TODO: implement alternative to extract user names as passwd is short and shadow file is blocked
# TODO: example: who | awk -F ' ' {'print $1'} | sort | uniq
passwd_filename = "/etc/passwd"
users_to_remove = {''}
groups_to_remove = {''}
### CODE
file = open(passwd_filename, "r")
users = set()
groups = set()
users_groups_dict = dict()
for line in file:
  # example entry from passwd
  # +misterX:::::/home/misterX:/bin/bash
  # split string
  line_splitted = str.split(line, ":")
  # extract and clean username
  username = line_splitted[0]
  username = username.replace("+", "")
  # add user to set
  users.add(username)
# remove some users
users = users.difference(users_to_remove)
#print(users)
# extract for each user all groups using groups
#users = list(users)[:20]
for user in users:
  #print(user)
  cmd = "groups " + user
  output = ""
  try:
    output = subprocess.check_output(cmd, shell=True)
    output = output.decode('ascii')
    output = output.replace("\n", "")
    # split username from groups
    output_splitted = str.split(output, ":")
    #print(output_splitted)
    users_groups = output_splitted[1]
    users_groups_splitted = str.split(users_groups, " ")
    users_groups_splitted_set = set(users_groups_splitted)
    users_groups_splitted_set = users_groups_splitted_set.difference(groups_to_remove)
    # add to pool of known groups
    groups = groups.union(users_groups_splitted_set)
    # add groups to dict
    users_groups_dict[user] = set(users_groups_splitted_set)
  except:
    # if user does not exist anymore (error) remove from set of users
    users = users.difference({user})
# print(users_groups_dict)
# create relationship matrix
header = "\t".join(str(x) for x in users)
content = "\t" + header + "\n"
groups_list = list(groups)
users_list = list(users)
for group in groups_list:
  content += group + "\t"
  for user in users_list:
    if group in users_groups_dict[user]:
      content += "1"
    else:
      content += "0"
    if not(groups_list.index(group) == len(groups_list)):
      content += "\t"
  content += "\n"
# final output
print(content)
