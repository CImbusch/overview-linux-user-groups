# overview-linux-user-groups

Main goal is to create a matrix with user to group associations. The information is extracted on /etc/passwd and multiple
groups commands such that it is not necessary to have root access rights to create such overview.

The repo contains a python scripts which creates a tsv file with user / groups relations. Sample R script which tries out some
visualizations of the created data.

python3.6 user_group_relation.py 
